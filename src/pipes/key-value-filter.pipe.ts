import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'keyValueFilter',
})
export class KeyValueFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Object.keys(value).map(function (key: any): any {
      let pair: any = {};
      let k: any = 'key';
      let v: any = 'value';

      pair[k] = key;
      pair[v] = value[key];
      return pair;
    });
  }

}
