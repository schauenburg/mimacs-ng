import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'qs-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {

  routes: Object[] = [{
    title: 'Home',
    route: '/',
    icon: 'home',
  }, {
    title: 'Lampsman Dashboard',
    route: '/lampsman',
    icon: 'dashboard',
  }, {
    title: 'Monitoring & Paging',
    route: '/paging',
    icon: 'message',
  }, {
    title: 'Repair Dashboard',
    route: '/repairs',
    icon: 'build',
  }, {
    title: 'Reports',
    route: '/reports',
    icon: 'view_column',
  }, {
    title: 'Admin Dashboard',
    route: '/admin',
    icon: 'fingerprint',
  },
  ];

  constructor(private _router: Router) {
  }

  logout(): void {
    this._router.navigate(['/login']);
  }
}
