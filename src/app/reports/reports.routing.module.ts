import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportsDashboardComponent } from './reports-dashboard/reports-dashboard.component';
import { MainComponent } from '../main/main.component';
import { LmsReportsComponent } from './lms-reports/lms-reports.component';
import { GdiReportsComponent } from './gdi-reports/gdi-reports.component';
import { MaintenanceReportsComponent } from './maintenance-reports/maintenance-reports.component';
import { ScasReportsComponent } from './scas-reports/scas-reports.component';
import { AmsReportsComponent } from './ams-reports/ams-reports.component';
import { AssetRegisterComponent } from './lms-reports/asset-register/asset-register.component';
import { StatusChangeComponent } from './lms-reports/status-change/status-change.component';
import { GasReportComponent } from './gdi-reports/gas-report/gas-report.component';
import { CalibrationReportComponent } from './gdi-reports/calibration-report/calibration-report.component';
import { TestReportComponent } from './gdi-reports/test-report/test-report.component';
import { TwaReportComponent } from './gdi-reports/twa-report/twa-report.component';
import { StelReportComponent } from './gdi-reports/stel-report/stel-report.component';
import { MovementReportComponent } from './ams-reports/movement-report/movement-report.component';
import { PagingReportComponent } from './ams-reports/paging-report/paging-report.component';
import { GateAccessReportComponent } from './ams-reports/gate-access-report/gate-access-report.component';
import { VehicleReportComponent } from './scas-reports/vehicle-report/vehicle-report.component';
import { CaplampReportComponent } from './scas-reports/caplamp-report/caplamp-report.component';

const reportRoutes: Routes = [
  {
    path: 'reports',
    component: MainComponent,
    canActivate: [],
    children: [
      {
        component: ReportsDashboardComponent,
        path: '',
        children: [
          {
            path: '',
            component: LmsReportsComponent,
            children: [
              {
                path: '',
                component: AssetRegisterComponent,
              },
              {
                path: 'lms/status',
                component: StatusChangeComponent,
              },
            ],
          },
          {
            path: 'gdi',
            component: GdiReportsComponent,
            children: [
              {
                path: '',
                component: GasReportComponent,
              },
              {
                path: 'calibration',
                component: CalibrationReportComponent,
              },
              {
                path: 'test',
                component: TestReportComponent,
              },
              {
                path: 'twa',
                component: TwaReportComponent,
              },
              {
                path: 'stel',
                component: StelReportComponent,
              },
            ],
          },
          {
            path: 'ams',
            component: AmsReportsComponent,
            children: [
              {
                path: '',
                component: MovementReportComponent,
              },
              {
                path: 'paging',
                component: PagingReportComponent,
              },
              {
                path: 'gate',
                component: GateAccessReportComponent,
              },
            ],
          },
          {
            path: 'scas',
            component: ScasReportsComponent,
            children: [
              {
                path: '',
                component: VehicleReportComponent,
              },
              {
                path: 'caplamp',
                component: CaplampReportComponent,
              },
            ],
          },
          {
            path: 'maintenance',
            component: MaintenanceReportsComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(reportRoutes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule {}
