import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AssetService } from '../../../../services/asset.service';
import { ToastsManager } from 'ng2-toastr';
import { IAssetType } from '../../../../models/asset-type';
import {
  IPageChangeEvent, ITdDataTableColumn, ITdDataTableSortChangeEvent, TdDataTableService,
  TdDataTableSortingOrder,
} from '@covalent/core';

@Component({
  selector: 'qs-asset-register',
  templateUrl: './asset-register.component.html',
  styleUrls: ['./asset-register.component.scss'],
})
export class AssetRegisterComponent implements OnInit {

  assetTypes: IAssetType[];
  data: any[] = [];

  columns: ITdDataTableColumn[] = [
    { name: 'equipmentType', label: 'Equipment Type'},
    { name: 'equipmentId', label: 'Equipment ID'},
    { name: 'serialNo', label: 'Serial No.'},
    { name: 'personId', label: 'Person ID', tooltip: 'Unique Mine No' },
    { name: 'name', label: 'Person Name' },
    { name: 'firstname', label: 'Firstname'},
    { name: 'occupation', label: 'Occupation'},
    { name: 'shaft', label: 'Shaft'},
    { name: 'shift', label: 'Shift'},
    { name: 'dateAssigned', label: 'Date Assigned'},
    { name: 'assignmentExpiryDate', label: 'Assignment Expiry'},
  ];

  filteredData: any[] = this.data;
  filteredTotal: number = this.data.length;

  selectable: boolean = false;
  clickable: boolean = true;
  searchTerm: string = '';
  fromRow: number = 1;
  currentPage: number = 1;
  pageSize: number = 5;
  sortBy: string = 'equipmentId';
  selectedRows: any[] = [];
  sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;

  constructor(private assetService: AssetService,
              private _dataTableService: TdDataTableService,
              public toastr: ToastsManager,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.loadAssetTypes();
    this.filter();
  }

  onSubmit(form: any): void {
    // do something
  }

  loadAssetTypes(): void {
    this.assetService.getEquipmentTypes()
      .subscribe((data: any[]) =>
          this.assetTypes = data,
        (error: any) => this.toastr.error(error));
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.filter();
  }

  filter(): void {
    let newData: any[] = this.data;
    let excludedColumns: string[] = this.columns
      .filter((column: ITdDataTableColumn) => {
        return ((column.filter === undefined && column.hidden === true) ||
        (column.filter !== undefined && column.filter === false));
      }).map((column: ITdDataTableColumn) => {
        return column.name;
      });
    newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
    this.filteredTotal = newData.length;
    newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData = newData;
  }

}
