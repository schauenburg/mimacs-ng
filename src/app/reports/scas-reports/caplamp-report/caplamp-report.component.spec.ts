import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaplampReportComponent } from './caplamp-report.component';

describe('CaplampReportComponent', () => {
  let component: CaplampReportComponent;
  let fixture: ComponentFixture<CaplampReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaplampReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaplampReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
