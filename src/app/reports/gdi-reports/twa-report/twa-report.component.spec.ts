import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwaReportComponent } from './twa-report.component';

describe('TwaReportComponent', () => {
  let component: TwaReportComponent;
  let fixture: ComponentFixture<TwaReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwaReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwaReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
