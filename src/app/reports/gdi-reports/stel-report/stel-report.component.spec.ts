import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StelReportComponent } from './stel-report.component';

describe('StelReportComponent', () => {
  let component: StelReportComponent;
  let fixture: ComponentFixture<StelReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StelReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StelReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
