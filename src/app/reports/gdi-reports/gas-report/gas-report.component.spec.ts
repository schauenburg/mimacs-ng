import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GasReportComponent } from './gas-report.component';

describe('GasReportComponent', () => {
  let component: GasReportComponent;
  let fixture: ComponentFixture<GasReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GasReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GasReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
