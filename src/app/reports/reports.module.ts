import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { ReportsRoutingModule } from './reports.routing.module';
import { ReportsDashboardComponent } from './reports-dashboard/reports-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { LmsReportsComponent } from './lms-reports/lms-reports.component';
import { GdiReportsComponent } from './gdi-reports/gdi-reports.component';
import { AmsReportsComponent } from './ams-reports/ams-reports.component';
import { ScasReportsComponent } from './scas-reports/scas-reports.component';
import { MaintenanceReportsComponent } from './maintenance-reports/maintenance-reports.component';
import { AssetRegisterComponent } from './lms-reports/asset-register/asset-register.component';
import { StatusChangeComponent } from './lms-reports/status-change/status-change.component';
import { GasReportComponent } from './gdi-reports/gas-report/gas-report.component';
import { CalibrationReportComponent } from './gdi-reports/calibration-report/calibration-report.component';
import { TestReportComponent } from './gdi-reports/test-report/test-report.component';
import { TwaReportComponent } from './gdi-reports/twa-report/twa-report.component';
import { StelReportComponent } from './gdi-reports/stel-report/stel-report.component';
import { MovementReportComponent } from './ams-reports/movement-report/movement-report.component';
import { PagingReportComponent } from './ams-reports/paging-report/paging-report.component';
import { GateAccessReportComponent } from './ams-reports/gate-access-report/gate-access-report.component';
import { VehicleReportComponent } from './scas-reports/vehicle-report/vehicle-report.component';
import { CaplampReportComponent } from './scas-reports/caplamp-report/caplamp-report.component';

@NgModule({
  declarations: [ReportsDashboardComponent, LmsReportsComponent, GdiReportsComponent, AmsReportsComponent, ScasReportsComponent, MaintenanceReportsComponent, AssetRegisterComponent, StatusChangeComponent, GasReportComponent, CalibrationReportComponent, TestReportComponent, TwaReportComponent, StelReportComponent, MovementReportComponent, PagingReportComponent, GateAccessReportComponent, VehicleReportComponent, CaplampReportComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    SharedModule,
    ReportsRoutingModule],
  providers: [],
})
export class ReportsModule {}
