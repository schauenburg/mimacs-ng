import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingReportComponent } from './paging-report.component';

describe('PagingReportComponent', () => {
  let component: PagingReportComponent;
  let fixture: ComponentFixture<PagingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
