import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GateAccessReportComponent } from './gate-access-report.component';

describe('GateAccessReportComponent', () => {
  let component: GateAccessReportComponent;
  let fixture: ComponentFixture<GateAccessReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GateAccessReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GateAccessReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
