import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementReportComponent } from './movement-report.component';

describe('MovementReportComponent', () => {
  let component: MovementReportComponent;
  let fixture: ComponentFixture<MovementReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
