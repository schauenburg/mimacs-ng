import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingOverviewComponent } from './paging-overview.component';

describe('PagingOverviewComponent', () => {
  let component: PagingOverviewComponent;
  let fixture: ComponentFixture<PagingOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagingOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
