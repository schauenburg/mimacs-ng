import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagingDashboardComponent } from './paging-dashboard.component';

describe('PagingDashboardComponent', () => {
  let component: PagingDashboardComponent;
  let fixture: ComponentFixture<PagingDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagingDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagingDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
