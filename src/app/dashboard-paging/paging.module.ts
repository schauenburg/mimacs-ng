import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { PagingRoutingModule } from './paging.routing.module';
import { PagingDashboardComponent } from './paging-dashboard/paging-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { PagingOverviewComponent } from './paging-overview/paging-overview.component';
import { PagingComponent } from './paging/paging.component';
import { MonitoringComponent } from './monitoring/monitoring.component';

@NgModule({
  declarations: [PagingDashboardComponent, PagingOverviewComponent, PagingComponent, MonitoringComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    SharedModule,
    PagingRoutingModule],
  providers: [],
})
export class PagingModule {}
