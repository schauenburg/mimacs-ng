import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagingDashboardComponent } from './paging-dashboard/paging-dashboard.component';
import { MainComponent } from '../main/main.component';
import {PagingOverviewComponent} from "./paging-overview/paging-overview.component";
import {PagingComponent} from "./paging/paging.component";
import {MonitoringComponent} from "./monitoring/monitoring.component";

const pagingRoutes: Routes = [
  {
    path: 'paging',
    component: MainComponent,
    canActivate: [],
    children: [
      {
        component: PagingDashboardComponent,
        path: '',
        children: [
          {
            path: '',
            component: PagingOverviewComponent,
            children: [
              {
                path: '',
                component: PagingComponent,
              },
              {
                path: 'monitoring',
                component: MonitoringComponent,
              },
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(pagingRoutes)],
  exports: [RouterModule],
})
export class PagingRoutingModule {}
