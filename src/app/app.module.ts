import { NgModule, Type } from '@angular/core';
import { BrowserModule, Title }  from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CovalentHttpModule } from '@covalent/http';
import { CovalentHighlightModule } from '@covalent/highlight';
import { CovalentMarkdownModule } from '@covalent/markdown';

import { AppComponent } from './app.component';
import { RequestInterceptor } from '../config/interceptors/request.interceptor';
import { MOCK_API } from '../config/api.config';

import { routedComponents, AppRoutingModule } from './app-routing.module';

import { SharedModule } from './shared/shared.module';

import { USER_PROVIDER, USERS_API } from './users';
import { HeaderComponent } from './layout/header/header.component';
import { MonitoringComponent } from './dashboard-admin/monitoring/monitoring.component';
import { AssetService } from '../services/asset.service';
import { PersonService } from '../services/person.service';
import { ServicesComponent } from './dashboard-admin/services/services.component';
import { ServicesService } from '../services/services.service';
import { TimelineComponent } from './dashboard-lampsman/timeline/timeline.component';
import { LookupService } from '../services/lookup.service';
import { ToastModule } from 'ng2-toastr';
import { ReportsModule } from './reports/reports.module';
import { PagingModule } from './dashboard-paging/paging.module';
import { RepairsModule } from './dashboard-repairs/repairs.module';
import { AdminModule } from './dashboard-admin/admin.module';
import { FooterComponent } from './layout/footer/footer.component';
import { LampsmanModule } from './dashboard-lampsman/lampsman.module';

const httpInterceptorProviders: Type<any>[] = [
  RequestInterceptor,
];

export function getAPI(): string {
  return MOCK_API;
}

@NgModule({
  declarations: [
    AppComponent,
    routedComponents,
    MonitoringComponent,
    ServicesComponent,
    TimelineComponent,
  ], // directives, components, and pipes owned by this NgModule
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    CovalentHttpModule.forRoot({
      interceptors: [{
        interceptor: RequestInterceptor, paths: ['**'],
      }],
    }),
    CovalentHighlightModule,
    CovalentMarkdownModule,
    ToastModule.forRoot(),
    ReportsModule,
    PagingModule,
    RepairsModule,
    AdminModule,
    LampsmanModule,
  ], // modules needed to run this module
  providers: [
    httpInterceptorProviders,
    Title, {
      provide: USERS_API, useFactory: getAPI,
    }, USER_PROVIDER,
    AssetService,
    PersonService,
    ServicesService,
    LookupService,
  ], // additional providers needed for this module
  entryComponents: [

  ],
  exports: [
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
