import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Person} from '../../../models/person';
import {PersonService} from '../../../services/person.service';

@Component({
  selector: 'qs-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss'],
})
export class PersonDetailsComponent implements OnInit {
  person: Person = undefined;

  constructor(private route: ActivatedRoute,
              private personService: PersonService) {
  }

  ngOnInit(): void {
    let key: string = 'id';
    this.route.params.subscribe((params: any) => {
      if (params[key]) {
        this.fetchPerson(params[key]);
      }
    });
  }

  fetchPerson(personId: string): void {
    this.personService.getPerson(personId)
      .subscribe((response: Person) => {
          this.person = response;
        });
  }
}
