import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {Person} from '../../../../models/person';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-measurements',
  templateUrl: './measurements.component.html',
  styleUrls: ['./measurements.component.scss'],
})
export class MeasurementsComponent implements OnInit {
  @Input() person: Person = undefined;
  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
