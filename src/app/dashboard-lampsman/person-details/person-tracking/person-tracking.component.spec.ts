import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonTrackingComponent } from './person-tracking.component';

describe('PersonTrackingComponent', () => {
  let component: PersonTrackingComponent;
  let fixture: ComponentFixture<PersonTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonTrackingComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
