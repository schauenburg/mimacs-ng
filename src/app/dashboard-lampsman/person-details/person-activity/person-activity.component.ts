import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {Person} from '../../../../models/person';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-person-activity',
  templateUrl: './person-activity.component.html',
  styleUrls: ['./person-activity.component.scss'],
})
export class PersonActivityComponent implements OnInit {
  @Input() person: Person = undefined;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
