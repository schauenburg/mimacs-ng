import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonActivityComponent } from './person-activity.component';

describe('PersonActivityComponent', () => {
  let component: PersonActivityComponent;
  let fixture: ComponentFixture<PersonActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonActivityComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
