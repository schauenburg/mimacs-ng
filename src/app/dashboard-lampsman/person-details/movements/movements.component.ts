import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {Person} from '../../../../models/person';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.scss'],
})
export class MovementsComponent implements OnInit {
  @Input() person: Person = undefined;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
