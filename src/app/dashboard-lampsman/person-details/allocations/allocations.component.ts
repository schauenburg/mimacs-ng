import {Component, Input, OnChanges, OnInit, ViewContainerRef} from '@angular/core';
import {Person} from '../../../../models/person';
import {Assignment} from '../../../../models/assignment';
import {PersonService} from '../../../../services/person.service';
import {TdLoadingService} from '@covalent/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-allocations',
  templateUrl: './allocations.component.html',
  styleUrls: ['./allocations.component.scss'],
})
export class AllocationsComponent implements OnInit, OnChanges {
  @Input() person: Person = undefined;
  allocations: Assignment[];

  constructor(private personService: PersonService,
              private loadingService: TdLoadingService,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.loadingService.register('allocations.load');
    this.fetchAllocationsForPerson(this.person);
  }

  ngOnChanges(changes: any): void {
    this.fetchAllocationsForPerson(changes.person.currentValue);
  }

  fetchAllocationsForPerson(person: Person): void {
    if (person !== undefined) {
      this.personService.getAssignmentsForPerson(this.person.personId)
        .subscribe((assignments: Assignment[]) => {
            this.allocations = assignments;
            setTimeout(() => {
              this.loadingService.resolve('allocations.load');
            }, 1000);
          },
          (error: any) => this.toastr.error(error));
    }
  }
}
