import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SearchComponent } from './search/search.component';
import { CreateComponent } from './create/create.component';
import { DashboardLampsmanComponent } from './dashboard-lampsman.component';
import { LampsmanOverviewComponent } from './overview/overview.component';
import { LampsmanStatsComponent } from './stats/stats.component';
import { AllocateComponent } from './allocate/allocate.component';
import { CreatePersonComponent } from './create/create-person/create-person.component';
import { CreateAssetComponent } from './create/create-asset/create-asset.component';
import { PersonSearchComponent } from './search/person-search/person-search.component';
import { AssetSearchComponent } from './search/asset-search/asset-search.component';
import { TimelineComponent } from './timeline/timeline.component';
import { EditPersonComponent } from './edit-person/edit-person.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { AssetTimelineComponent } from './asset-timeline/asset-timeline.component';
import { EditAssetComponent } from './edit-asset/edit-asset.component';
import { AssetDetailsComponent } from './asset-details/asset-details.component';
import { MainComponent } from '../main/main.component';

const lampsmanRoutes: Routes = [
  {
    path: 'lampsman',
    component: MainComponent,
    children: [
      {
        path: '',
        component: DashboardLampsmanComponent,
        children: [
          {
            path: '',
            component: LampsmanOverviewComponent,
          },
          {
            path: 'search',
            component: SearchComponent,
            children: [
              {
                path: '',
                component: PersonSearchComponent,
              },
              {
                path: 'asset',
                component: AssetSearchComponent,
              },
            ],
          },
          {
            path: 'allocate',
            component: AllocateComponent,
          },
          {
            path: 'create',
            component: CreateComponent,
            children: [
              {
                path: '',
                component: CreatePersonComponent,
              },
              {
                path: 'asset',
                component: CreateAssetComponent,
              },
            ],
          },
          {
            path: 'person/timeline/:id',
            component: TimelineComponent,
          },
          {
            path: 'person/edit/:id',
            component: EditPersonComponent,
          },
          {
            path: 'person/details/:id',
            component: PersonDetailsComponent,
          },
          {
            path: 'asset/timeline/:id',
            component: AssetTimelineComponent,
          },
          {
            path: 'asset/edit/:id',
            component: EditAssetComponent,
          },
          {
            path: 'asset/details/:id',
            component: AssetDetailsComponent,
          },
        ],
      },
      {
        path: 'stats',
        component: LampsmanStatsComponent,
      },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(lampsmanRoutes)],
  exports: [RouterModule],
})
export class LampsmanRoutingModule {}
