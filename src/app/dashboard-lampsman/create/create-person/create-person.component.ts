import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewContainerRef} from '@angular/core';
import {StepState, TdMediaService} from '@covalent/core';
import {IAttribute} from '../../../../models/attribute';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {KeyValueFilterPipe} from '../../../../pipes/key-value-filter.pipe';
import {IAttributeMapEntry} from '../../../../models/attribute-map-entry';
import {Person} from '../../../../models/person';
import {PersonService} from '../../../../services/person.service';
import {IPersonType} from '../../../../models/person-type';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.scss'],
})
export class CreatePersonComponent implements AfterViewInit, OnInit {

  activeDeactiveStep1Msg: string = 'No select/deselect detected yet';
  stateStep1: StepState = StepState.None;
  stateStep2: StepState = StepState.None;
  stateStep3: StepState = StepState.None;
  disabled: boolean = false;
  disabledStep3: boolean = true;

  activeStep1: boolean = true;
  activeStep2: boolean = false;
  activeStep3: boolean = false;

  personTypeAttributes: IAttribute[] = [];
  personTypes: IPersonType[];
  selectedPersonType: IPersonType;

  public createPersonForm: FormGroup;

  createdPerson: Person = undefined;

  constructor(public media: TdMediaService,
              private _changeDetectorRef: ChangeDetectorRef,
              private personService: PersonService,
              private  _fb: FormBuilder,
              public toastr: ToastsManager,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.loadPersonTypes();
    this.initFormModel();
  }

  ngAfterViewInit(): void {
    // broadcast to all listener observables when loading the page
    this.media.broadcast();
    // force a new change detection cycle since change detections
    // have finished when `ngAfterViewInit` is executed
    this._changeDetectorRef.detectChanges();
  }

  toggleRequiredStep2(): void {
    this.stateStep2 = (this.stateStep2 === StepState.Required ? StepState.None : StepState.Required);
  }

  toggleCompleteStep3(): void {
    this.stateStep3 = (this.stateStep3 === StepState.Complete ? StepState.None : StepState.Complete);
  }

  toggleDisabled(): void {
    this.disabled = !this.disabled;
  }

  activeStep1Event(): void {
    this.activeDeactiveStep1Msg = 'Active event emitted.';
  }

  activeStep3Event(): void {
    if (this.stateStep1 !== StepState.Complete) {
      this.activeStep1 = true;
      this.stateStep1 = StepState.Required;
    }
  }

  deactiveStep1Event(): void {
    this.activeDeactiveStep1Msg = 'Deactive event emitted.';
  }

  deactiveStep2Event(formValid: boolean): void {
    if (!formValid) {
      this.stateStep2 = StepState.Required;
    }
  }

  createPerson(model: Person): any {
    let map: any[] = <IAttributeMapEntry[]>new KeyValueFilterPipe().transform(model.attributeMap);
    model.type = this.selectedPersonType.name;
    model.attributeMap = map;
    // let postData = JSON.stringify(model);
    this.personService.createPerson(model)
      .subscribe((response: Person) => {
          this.createdPerson = response;
          // this.toastr.success('You are awesome, you created a new person!', 'Success!');
        },
        ((error: any) => this.toastr.error(error)),
        () => {
          this.toastr.success('You are awesome, you created a new person!', 'Success!');
          this.stateStep2 = StepState.Complete;
          this.activeStep2 = false;
          this.activeStep3 = true;
          this.disabledStep3 = false;
          this.stateStep3 = StepState.Complete;
        });
  }

  loadPersonTypes(): void {
    this.personService.getPersonTypes()
      .subscribe((data: any[]) =>
          this.personTypes = data,
        (error: any) => this.toastr.error(error, 'Error!'),
        () => {
          // this.personTypeSelected(this.personTypes[0]);
        });
  }

  personTypeSelected(type: IPersonType): void {
    this.selectedPersonType = type;
    this.getPersonTypeAttributes(type.name);
    this.activeStep1 = false;
    this.stateStep1 = StepState.Complete;
    this.activeStep2 = true;

  }

  getPersonTypeAttributes(name: string): void {
    this.personService.getPersonTypeAttributes(name)
      .subscribe((attributes: any[]) => {
          this.personTypeAttributes = attributes;
        },
        (error: any) => this.toastr.error(error),
        () => {
          this.initFormModel();
        });
  }

  attributeMapFormGroup(): FormGroup {
    let attribFormGroup: FormGroup = new FormGroup({});
    for (let attrib of this.personTypeAttributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  initFormModel(): void {
    this.createPersonForm = this._fb.group({
      personId: ['', [Validators.required]],
      type: [this.selectedPersonType, [Validators.required]],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  resetForm(): void {
    this.createPersonForm.reset();
    this.createdPerson = undefined;
    this.selectedPersonType = undefined;
    this.stateStep1 = StepState.None;
    this.stateStep2 = StepState.None;
    this.stateStep3 = StepState.None;
    this.disabledStep3 = true;
    this.activeStep1 = true;
    this.initFormModel();
  }

}
