import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IAttribute} from '../../../../../models/attribute';
import {IAssetType} from '../../../../../models/asset-type';
import {Asset} from '../../../../../models/asset';
import {ToastsManager} from 'ng2-toastr';
import {AssetService} from '../../../../../services/asset.service';

@Component({
  selector: 'qs-create-asset-form',
  templateUrl: './create-asset-form.component.html',
  styleUrls: ['./create-asset-form.component.scss'],
})
export class CreateAssetFormComponent implements OnInit {
  public createAssetForm: FormGroup;
  assetTypeAttributes: IAttribute[] = [];
  @Input() assetType: IAssetType;
  createdAsset: Asset;

  constructor(private formBuilder: FormBuilder,
              public toastr: ToastsManager,
              private assetService: AssetService) {
  }

  ngOnInit(): void {
    alert(this.assetType.description);
    this.getAssetTypeAttributes(this.assetType.name);
    this.initFormModel();
  }

  initFormModel(): void {
    this.createAssetForm = this.formBuilder.group({
      assetId: ['', [Validators.required]],
      type: [this.assetType],
      status: ['operational'],
      allocStatus: ['unallocated'],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  attributeMapFormGroup(): FormGroup {
    let attribFormGroup: FormGroup = new FormGroup({});
    for (let attrib of this.assetTypeAttributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  getAssetTypeAttributes(name: string): void {
    this.assetService.getEquipmentTypeAttributes(name)
      .subscribe((attributes: any[]) => {
          this.assetTypeAttributes = attributes;
        },
        (error: any) => this.toastr.error(error),
        () => {
          this.initFormModel();
        });
  }
}
