import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAssetFormComponent } from './create-asset-form.component';

describe('CreateAssetFormComponent', () => {
  let component: CreateAssetFormComponent;
  let fixture: ComponentFixture<CreateAssetFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAssetFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAssetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
