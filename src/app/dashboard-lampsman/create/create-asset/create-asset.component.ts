import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { IAssetType } from '../../../../models/asset-type';
import { AssetService } from '../../../../services/asset.service';
import { IAttribute } from '../../../../models/attribute';
import { IAttributeMapEntry } from '../../../../models/attribute-map-entry';
import { Asset } from '../../../../models/asset';
import { KeyValueFilterPipe } from '../../../../pipes/key-value-filter.pipe';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StepState } from '@covalent/core';
import { ISelectItem } from '../../../../models/select-item';
import { LookupService } from '../../../../services/lookup.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'qs-create-asset',
  templateUrl: './create-asset.component.html',
  styleUrls: ['./create-asset.component.scss'],
})
export class CreateAssetComponent implements OnInit {
  assetTypes: IAssetType[];
  assetTypeAttributes: IAttribute[] = [];
  selectedAssetType: IAssetType;
  createdAsset: Asset;

  activeDeactiveStep1Msg: string = 'No select/deselect detected yet';
  stateStep1: StepState = StepState.None;
  stateStep2: StepState = StepState.None;
  stateStep3: StepState = StepState.None;
  disabled: boolean = false;
  disabledStep3: boolean = true;

  activeStep1: boolean = true;
  activeStep2: boolean = false;
  activeStep3: boolean = false;

  public createAssetForm: FormGroup;

  constructor(private assetService: AssetService,
              private lookupService: LookupService,
              public toastr: ToastsManager,
              private _fb: FormBuilder,
              private vcr: ViewContainerRef) {
    this
      .toastr
      .setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.loadAssetTypes();
    this.initFormModel();
  }

  createAsset(model: Asset): any {
    let map: any[] = <IAttributeMapEntry[]>new KeyValueFilterPipe().transform(model.attributeMap);
    model.type = this.selectedAssetType.name;
    model.attributeMap = map;
    this.assetService.createEquipment(model).subscribe(
      (response: Asset) => {
        this.createdAsset = response;
      },
      (error: any) => this.toastr.error(error),
      () => {
        this.toastr.success('You are awesome, you created a new asset!', 'Success!');
        this.stateStep2 = StepState.Complete;
        this.activeStep2 = false;
        this.activeStep3 = true;
        this.disabledStep3 = false;
        this.stateStep3 = StepState.Complete;
      },
    );
  }

  loadAssetTypes(): void {
    this.assetService.getEquipmentTypes()
      .subscribe((data: any[]) =>
          this.assetTypes = data,
        (error: any) => this.toastr.error(error));
  }

  assetTypeSelected(type: any): void {
    this.selectedAssetType = type;
    this.getAssetTypeAttributes(type.name);
    this.activeStep1 = false;
    this.stateStep1 = StepState.Complete;
    this.activeStep2 = true;
  }

  getAssetTypeAttributes(name: string): void {
    this.assetService.getEquipmentTypeAttributes(name)
      .subscribe((attributes: any[]) => {
          this.assetTypeAttributes = attributes;
        },
        (error: any) => this.toastr.error(error),
        () => {
          this.initFormModel();
        });
  }

  attributeMapFormGroup(): FormGroup {
    let attribFormGroup: FormGroup = new FormGroup({});
    for (let attrib of this.assetTypeAttributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  initFormModel(): void {
    this.createAssetForm = this._fb.group({
      assetId: ['', [Validators.required]],
      type: [this.selectedAssetType],
      status: ['operational'],
      allocStatus: ['unallocated'],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  activeStep1Event(): void {
    this.activeDeactiveStep1Msg = 'Active event emitted.';
  }

  deactiveStep1Event(): void {
    this.activeDeactiveStep1Msg = 'Deactive event emitted.';
  }

  deactiveStep2Event(formValid: boolean): void {
    if (!formValid) {
      this.stateStep2 = StepState.Required;
    }
  }

  getSelectItems(lookup: string): ISelectItem[] {
    let selectItems: ISelectItem [];
    this.lookupService.getLookupList(lookup)
      .subscribe((response: any[]) => selectItems = response,
        (error: any) => this.toastr.error(error));
    return selectItems;
  }
}
