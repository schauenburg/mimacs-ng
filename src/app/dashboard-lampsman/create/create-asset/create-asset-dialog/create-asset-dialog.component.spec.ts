import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAssetDialogComponent } from './create-asset-dialog.component';

describe('CreateAssetDialogComponent', () => {
  let component: CreateAssetDialogComponent;
  let fixture: ComponentFixture<CreateAssetDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAssetDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAssetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
