import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IAttribute } from '../../../../../models/attribute';
import { IAssetType } from '../../../../../models/asset-type';
import { Asset } from '../../../../../models/asset';
import { AssetService } from '../../../../../services/asset.service';
import { ToastsManager } from 'ng2-toastr';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { IAttributeMapEntry } from '../../../../../models/attribute-map-entry';
import { KeyValueFilterPipe } from '../../../../../pipes/key-value-filter.pipe';

@Component({
  selector: 'qs-create-asset-dialog',
  templateUrl: './create-asset-dialog.component.html',
  styleUrls: ['./create-asset-dialog.component.scss'],
})
export class CreateAssetDialogComponent implements OnInit {

  public createAssetForm: FormGroup;
  assetTypeAttributes: IAttribute[] = [];
  assetType: IAssetType;
  createdAsset: Asset;

  constructor(private formBuilder: FormBuilder,
              public toastr: ToastsManager,
              private assetService: AssetService,
              @Inject(MD_DIALOG_DATA) public data: any,
              @Inject(MD_DIALOG_DATA) public height: any,
              @Inject(MD_DIALOG_DATA) public width: any,
              private dialogRef: MdDialogRef<CreateAssetDialogComponent>) {
  }

  ngOnInit(): void {
    this.getAssetType(this.data);
    this.getAssetTypeAttributes(this.data);
    this.initFormModel();
  }

  createAsset(model: Asset): any {
    let map: any[] = <IAttributeMapEntry[]>new KeyValueFilterPipe().transform(model.attributeMap);
    model.type = this.assetType.name;
    model.attributeMap = map;
    this.assetService.createEquipment(model).subscribe(
      (response: Asset) => {
        this.createdAsset = response;
        this.dialogRef.close({asset: this.createdAsset});
      },
      (error: any) => this.toastr.error(error),
      () => {
        this.toastr.success('You are awesome, you created a new asset!', 'Success!');
      },
    );
  }

  initFormModel(): void {
    this.createAssetForm = this.formBuilder.group({
      assetId: ['', [Validators.required]],
      type: [this.assetType],
      status: ['operational'],
      allocStatus: ['unallocated'],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  attributeMapFormGroup(): FormGroup {
    let attribFormGroup: FormGroup = new FormGroup({});
    for (let attrib of this.assetTypeAttributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  getAssetType(name: string): void {
    this.assetService.getEquipmentTypeByName(name)
      .subscribe((type: any) => this.assetType = type);
  }

  getAssetTypeAttributes(name: string): void {
    this.assetService.getEquipmentTypeAttributes(name)
      .subscribe((attributes: any[]) => {
          this.assetTypeAttributes = attributes;
        },
        (error: any) => this.toastr.error(error),
        () => {
          this.initFormModel();
        });
  }
}
