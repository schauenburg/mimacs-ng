import {Component, Inject} from '@angular/core';
import {MD_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'qs-asset-summary-dialog',
  templateUrl: './asset-summary-dialog.component.html',
  styleUrls: ['./asset-summary-dialog.component.scss'],
})
export class AssetSummaryDialogComponent {

  constructor(@Inject(MD_DIALOG_DATA) public data: any) { }

}
