import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSummaryDialogComponent } from './asset-summary-dialog.component';

describe('AssetSummaryDialogComponent', () => {
  let component: AssetSummaryDialogComponent;
  let fixture: ComponentFixture<AssetSummaryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetSummaryDialogComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSummaryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
