import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetTimelineComponent } from './asset-timeline.component';

describe('AssetTimelineComponent', () => {
  let component: AssetTimelineComponent;
  let fixture: ComponentFixture<AssetTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetTimelineComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
