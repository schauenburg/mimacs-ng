import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-asset-timeline',
  templateUrl: './asset-timeline.component.html',
  styleUrls: ['./asset-timeline.component.scss'],
})
export class AssetTimelineComponent implements OnInit {

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
