import { Component, Input, OnChanges, SimpleChange, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Assignment } from '../../../../models/assignment';
import { Person } from '../../../../models/person';

@Component({
  selector: 'qs-misc',
  templateUrl: './misc.component.html',
  styleUrls: ['./misc.component.scss'],
})
export class MiscComponent implements OnChanges {
  equipmentState: string = 'misc unassigned';
  @Input() assignedMisc: Assignment[];
  @Input() person: Person;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if(changes['assignedMisc'] && this.assignedMisc.length > 0) {
      this.equipmentState = 'misc assigned';
    }
    if(changes['assignedMisc'] && this.assignedMisc.length === 0) {
      this.equipmentState = 'misc unassigned';
    }
  }
}
