import { Component, Input, OnChanges, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Person } from '../../../models/person';
import { Assignment } from '../../../models/assignment';
import { PersonService } from '../../../services/person.service';

@Component({
  selector: 'qs-minerbody',
  templateUrl: './minerbody.component.html',
  styleUrls: ['./minerbody.component.scss'],
})
export class MinerbodyComponent implements OnInit, OnDestroy {
  @Input() person: Person;
  assignments: Assignment[] = [];
  assignedCaplamps: Assignment[] = [];
  assignedGdis: Assignment[] = [];
  assignedRescuePacks: Assignment[] = [];
  assignedPds: Assignment[] = [];
  assignedMisc: Assignment[] = [];
  subscription: any;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef,
              private personService: PersonService) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.subscription = this.personService.personAssignments.subscribe(
      (selection: Assignment[]) => {
        this.assignments = selection;
        this.processAssignments(this.assignments);
      },
    );
  }

  processAssignments(assignments: Assignment[]): void {
    this.assignedCaplamps = [];
    this.assignedGdis = [];
    this.assignedRescuePacks = [];
    for (let assignment of assignments) {
      if (assignment.asset.assetType.name === 'CapLamp') {
        this.assignedCaplamps.push(assignment);
      }
      else if (assignment.asset.assetType.name === 'GasInstrument' || assignment.asset.assetType.name === 'sentinel') {
        this.assignedGdis.push(assignment);
      }
      else if (assignment.asset.assetType.name === 'RescuePack') {
        this.assignedRescuePacks.push(assignment);
      }
      else {
        this.assignedMisc.push(assignment);
      }
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
