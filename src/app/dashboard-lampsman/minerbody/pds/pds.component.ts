import { Component, Input, OnChanges, SimpleChange, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Person } from '../../../../models/person';
import { Assignment } from '../../../../models/assignment';

@Component({
  selector: 'qs-pds',
  templateUrl: './pds.component.html',
  styleUrls: ['./pds.component.scss'],
})
export class PdsComponent implements OnChanges {
  equipmentState: string = 'pds unassigned';
  @Input() assignedPds: Assignment[];
  @Input() person: Person;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedPds'] && this.assignedPds.length > 0) {
      this.equipmentState = 'pds assigned';
    }
    if (changes['assignedPds'] && this.assignedPds.length === 0) {
      this.equipmentState = 'pds unassigned';
    }
  }
}
