import { Component, Input, OnChanges, SimpleChange, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Assignment } from '../../../../models/assignment';
import { Person } from '../../../../models/person';

@Component({
  selector: 'qs-caplamp',
  templateUrl: './caplamp.component.html',
  styleUrls: ['./caplamp.component.scss'],
})
export class CaplampComponent implements OnChanges {
  equipmentState: string = 'cap-lamp unassigned';
  @Input() assignedCaplamps: Assignment[];
  @Input() person: Person;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if(changes['assignedCaplamps'] && this.assignedCaplamps.length > 0) {
      this.equipmentState = 'cap-lamp assigned';
    }
    if(changes['assignedCaplamps'] && this.assignedCaplamps.length === 0) {
      this.equipmentState = 'cap-lamp unassigned';
    }
  }
}
