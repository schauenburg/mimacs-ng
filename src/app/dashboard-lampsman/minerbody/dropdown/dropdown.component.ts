import { Component, Input, ViewContainerRef } from '@angular/core';
import { AssetService } from '../../../../services/asset.service';
import { PersonService } from '../../../../services/person.service';
import { ToastsManager } from 'ng2-toastr';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { Assignment } from '../../../../models/assignment';
import { Asset } from '../../../../models/asset';
import { AllocateDialogComponent } from '../../allocate/allocate-dialog/allocate-dialog.component';
import { CreateAssetDialogComponent } from '../../create/create-asset/create-asset-dialog/create-asset-dialog.component';
import { Person } from '../../../../models/person';
import { AllocateConfirmationDialogComponent } from '../../allocate/allocate-confirmation-dialog/allocate-confirmation-dialog.component';

@Component({
  selector: 'qs-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent {
  operationalStatus: string = 'Operational';
  @Input() assignments: Assignment[];
  @Input() person: Person;
  @Input() assetType: any;
  shiftOptions: string[] = ['Morning', 'Afternoon', 'Evening'];

  constructor(private assetService: AssetService,
              private personService: PersonService,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef,
              public dialog: MdDialog) {
    toastr.setRootViewContainerRef(vcr);
    }

  openCreateAssetDialog(): any {
    let createdAsset: Asset;
    let dialogRef: any = this.dialog.open(CreateAssetDialogComponent, <MdDialogConfig>{
      width: '50vw',
      data: this.assetType,
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      createdAsset = result.asset;
      if (createdAsset && this.person) {
        this.openAllocateConfirmationDialog(createdAsset);
      }
    });
  }

  openAllocateConfirmationDialog(asset: Asset): any {
    let assign: string;
    let dialogRef: any = this.dialog.open(AllocateConfirmationDialogComponent, <MdDialogConfig>{
      width: '30vw',
      data: this.person.name,
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      assign = result;
      if (assign === 'true') {
        this.assignAsset(asset);
      }
    });
  }

  deallocate(assignment: Assignment): void {
    this.assetService.deassignEquipment(assignment)
      .subscribe((assignments: Assignment[]) => {
          this.personService.updatePersonAssignments(assignments);
        },
        (error: any) => this.toastr.error(error),
        () => this.toastr.success('Asset was successfully deallocated', 'Success!'));
  }

  openAvailableAssetsDialog(input: string[]): void {
    let selectedAsset: Asset;
    input.push(this.assetType);
    this.dialog.open(AllocateDialogComponent, <MdDialogConfig>{
      width: '50vw',
      data: input,
    }).afterClosed()
      .subscribe((response: any) => {
        if (response) {
          selectedAsset = response.asset;
          this.assignAsset(selectedAsset);
        }
      });
  }

  assignAsset(asset: Asset): void {
    if (this.person !== undefined && asset !== undefined) {
      let assignment: Assignment = new Assignment(asset, this.person);
      this.assetService.assignEquipment(assignment)
        .subscribe((response: Assignment[]) => this.personService.updatePersonAssignments(response),
          (error2: any) => this.toastr.error(error2),
          () => {
            this.toastr.success('Allocation was successful', 'Success!');
          });
    }
  }
}
