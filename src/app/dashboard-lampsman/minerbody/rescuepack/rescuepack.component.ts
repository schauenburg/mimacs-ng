import { Component, Input, OnChanges, SimpleChange, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Assignment } from '../../../../models/assignment';
import { Person } from '../../../../models/person';

@Component({
  selector: 'qs-rescuepack',
  templateUrl: './rescuepack.component.html',
  styleUrls: ['./rescuepack.component.scss'],
})
export class RescuepackComponent implements OnChanges {
  equipmentState: string = 'rescue-pack unassigned';
  @Input() assignedRescuePacks: Assignment[];
  @Input() person: Person;
  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedRescuePacks'] && this.assignedRescuePacks.length > 0) {
      this.equipmentState = 'rescue-pack assigned';
    }
    if (changes['assignedRescuePacks'] && this.assignedRescuePacks.length === 0) {
      this.equipmentState = 'rescue-pack unassigned';
    }
  }

}
