import { Component, Input, OnChanges, SimpleChange, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr';
import { Assignment } from '../../../../models/assignment';
import { Person } from '../../../../models/person';

@Component({
  selector: 'qs-gdi',
  templateUrl: './gdi.component.html',
  styleUrls: ['./gdi.component.scss'],
})
export class GdiComponent implements OnChanges {
  equipmentState: string = 'gdi unassigned';
  @Input() assignedGdis: Assignment[];
  @Input() person: Person;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedGdis'] && this.assignedGdis.length > 0) {
      this.equipmentState = 'gdi assigned';
    }
    if (changes['assignedGdis'] && this.assignedGdis.length === 0) {
      this.equipmentState = 'gdi unassigned';
    }
  }

}
