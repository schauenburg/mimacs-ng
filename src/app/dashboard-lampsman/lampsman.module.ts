import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { LampsmanRoutingModule } from './lampsman.routing.module';
import { NgModule } from '@angular/core';
import { SearchComponent } from './search/search.component';
import { CreateComponent } from './create/create.component';
import { PassiveTagReaderComponent } from './passive-tag-reader/passive-tag-reader.component';
import { ActiveTagReaderComponent } from './active-tag-reader/active-tag-reader.component';
import { AssetService } from '../../services/asset.service';
import { GdiComponent } from './minerbody/gdi/gdi.component';
import { PersonService } from '../../services/person.service';
import { RescuepackComponent } from './minerbody/rescuepack/rescuepack.component';
import { CaplampComponent } from './minerbody/caplamp/caplamp.component';
import { PdsComponent } from './minerbody/pds/pds.component';
import { MiscComponent } from './minerbody/misc/misc.component';
import { CreatePersonComponent } from './create/create-person/create-person.component';
import { MinerbodyComponent } from './minerbody/minerbody.component';
import { ExpiryblockComponent } from './expiryblock/expiryblock.component';
import { PersonSearchComponent } from './search/person-search/person-search.component';
import { DashboardLampsmanComponent } from './dashboard-lampsman.component';
import { AssetSearchComponent } from './search/asset-search/asset-search.component';
import { AssetSearchDialogComponent } from './search/asset-search/asset-search-dialog/asset-search-dialog.component';
import { AssetSearchFormComponent } from './search/asset-search/asset-search-form/asset-search-form.component';
import { PersonSearchDialogComponent } from './search/person-search/person-search-dialog/person-search-dialog.component';
import { PersonSearchFormComponent } from './search/person-search/person-search-form/person-search-form.component';
import { RecentlyDeallocatedComponent } from './allocate/recently-deallocated/recently-deallocated.component';
import { RecentlyAllocatedComponent } from './allocate/recently-allocated/recently-allocated.component';
import { CreateAssetComponent } from './create/create-asset/create-asset.component';
import { CreateAssetDialogComponent } from './create/create-asset/create-asset-dialog/create-asset-dialog.component';
import { CreateAssetFormComponent } from './create/create-asset/create-asset-form/create-asset-form.component';
import { AllocateComponent } from './allocate/allocate.component';
import { AllocateDialogComponent } from './allocate/allocate-dialog/allocate-dialog.component';
import { AllocateConfirmationDialogComponent } from './allocate/allocate-confirmation-dialog/allocate-confirmation-dialog.component';
import { AssetDetailsComponent } from './asset-details/asset-details.component';
import { AssetSummaryDialogComponent } from './asset-summary-dialog/asset-summary-dialog.component';
import { AssetTimelineComponent } from './asset-timeline/asset-timeline.component';
import { EditAssetComponent } from './edit-asset/edit-asset.component';
import { EditPersonComponent } from './edit-person/edit-person.component';
import { LampsmanOverviewComponent } from './overview/overview.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { AllocationsComponent } from './person-details/allocations/allocations.component';
import { MeasurementsComponent } from './person-details/measurements/measurements.component';
import { PersonActivityComponent } from './person-details/person-activity/person-activity.component';
import { PersonTrackingComponent } from './person-details/person-tracking/person-tracking.component';
import { LookupService } from '../../services/lookup.service';
import { LampsmanStatsComponent } from './stats/stats.component';
import { DropdownComponent } from './minerbody/dropdown/dropdown.component';
import { SharedModule } from '../shared/shared.module';
import { KeyValueFilterPipe } from '../../pipes/key-value-filter.pipe';
import { MovementsComponent } from './person-details/movements/movements.component';

@NgModule({
  declarations: [
    DashboardLampsmanComponent,
    SearchComponent,
    CreateComponent,
    AssetSearchComponent,
    AssetSearchDialogComponent,
    AssetSearchFormComponent,
    ExpiryblockComponent,
    MinerbodyComponent,
    PersonSearchComponent,
    PersonSearchDialogComponent,
    PersonSearchFormComponent,
    RecentlyAllocatedComponent,
    RecentlyDeallocatedComponent,
    PassiveTagReaderComponent,
    ActiveTagReaderComponent,
    CreatePersonComponent,
    CreateAssetComponent,
    CreateAssetDialogComponent,
    CreateAssetFormComponent,
    MiscComponent,
    PdsComponent,
    CaplampComponent,
    RescuepackComponent,
    GdiComponent,
    AllocateComponent,
    AllocateDialogComponent,
    AllocateConfirmationDialogComponent,
    AssetDetailsComponent,
    AssetSummaryDialogComponent,
    AssetTimelineComponent,
    EditAssetComponent,
    EditPersonComponent,
    LampsmanOverviewComponent,
    LampsmanStatsComponent,
    PersonDetailsComponent,
    AllocationsComponent,
    MeasurementsComponent,
    PersonActivityComponent,
    PersonTrackingComponent,
    DropdownComponent,
    KeyValueFilterPipe,
    MovementsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    SharedModule,
    LampsmanRoutingModule,
  ],
  providers: [
    AssetService,
    PersonService,
    LookupService,
  ],
  entryComponents: [
    AssetSummaryDialogComponent,
    AssetSearchDialogComponent,
    CreateAssetDialogComponent,
    PersonSearchDialogComponent,
    AllocateDialogComponent,
  ],
})
export class LampsmanModule {}
