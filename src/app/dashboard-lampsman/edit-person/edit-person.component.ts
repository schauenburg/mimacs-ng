import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {PersonService} from '../../../services/person.service';
import {Person} from '../../../models/person';
import {IAttribute} from '../../../models/attribute';
import {IAttributeMapEntry} from '../../../models/attribute-map-entry';
import {KeyValueFilterPipe} from '../../../pipes/key-value-filter.pipe';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.scss'],
})
export class EditPersonComponent implements OnInit {
  public editPersonForm: FormGroup;

  person: Person = undefined;
  personTypeAttributes: IAttribute[] = [];

  constructor(private route: ActivatedRoute,
              private personService: PersonService,
              private  _fb: FormBuilder,
              public toastr: ToastsManager,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    let key: string = 'id';
    this.route.params.subscribe((params: any) => {
      if (params[key]) {
        this.fetchPerson(params[key]);
      }
    });
  }

  fetchPerson(personId: string): void {
    this.personService.getPerson(personId)
      .subscribe((response: Person) => {
        this.person = response;
        this.getPersonTypeAttributes(this.person.type.name);
        this.initFormModel();
      });
  }

  getPersonTypeAttributes(name: string): void {
    this.personService.getPersonTypeAttributes(name)
      .subscribe((attributes: any[]) => {
          this.personTypeAttributes = attributes;
        },
        (error: any) => this.toastr.error(error),
        () => {
          this.initFormModel();
        });
  }

  editPerson(model: Person): any {
    let map: any[] = <IAttributeMapEntry[]>new KeyValueFilterPipe().transform(model.attributeMap);
    model.type = this.person.type.name;
    model.personId = this.person.personId;
    model.attributeMap = map;
    this.personService.editPerson(model)
      .subscribe((response: Person) => {
          this.person = response;
        },
        ((error: any) => this.toastr.error(error)),
        () => {
          this.toastr.success('You are awesome, you updated a person!', 'Success!');
        });
  }

  attributeMapFormGroup(): FormGroup {
    let attribFormGroup: FormGroup = new FormGroup({});
    for (let attrib of this.person.attr) {
      attribFormGroup.addControl(attrib.name, new FormControl(attrib.value));
    }
    return attribFormGroup;
  }

  initFormModel(): void {
    this.editPersonForm = this._fb.group({
      personId: new FormControl({value: this.person.personId, disabled: true}),
      type: new FormControl({value: this.person.type, disabled: true}),
      attributeMap: this.attributeMapFormGroup(),
    });
  }

}
