import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassiveTagReaderComponent } from './passive-tag-reader.component';

describe('PassiveTagReaderComponent', () => {
  let component: PassiveTagReaderComponent;
  let fixture: ComponentFixture<PassiveTagReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassiveTagReaderComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassiveTagReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
