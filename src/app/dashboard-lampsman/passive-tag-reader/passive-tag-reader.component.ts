import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-passive-tag-reader',
  templateUrl: './passive-tag-reader.component.html',
  styleUrls: ['./passive-tag-reader.component.scss'],
})
export class PassiveTagReaderComponent implements OnInit {

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
