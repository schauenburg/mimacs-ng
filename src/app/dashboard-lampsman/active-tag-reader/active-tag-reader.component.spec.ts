import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveTagReaderComponent } from './active-tag-reader.component';

describe('ActiveTagReaderComponent', () => {
  let component: ActiveTagReaderComponent;
  let fixture: ComponentFixture<ActiveTagReaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveTagReaderComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveTagReaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
