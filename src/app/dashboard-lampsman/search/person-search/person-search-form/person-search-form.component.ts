import {Component, EventEmitter, OnInit, Output, ViewContainerRef} from '@angular/core';
import {Person} from '../../../../../models/person';
import {PersonService} from '../../../../../services/person.service';
import {ToastsManager} from 'ng2-toastr';
import {IPersonType} from '../../../../../models/person-type';
import {
  IPageChangeEvent,
  ITdDataTableColumn, ITdDataTableSortChangeEvent, TdDataTableService,
  TdDataTableSortingOrder,
} from '@covalent/core';

@Component({
  selector: 'qs-person-search-form',
  templateUrl: './person-search-form.component.html',
  styleUrls: ['./person-search-form.component.scss'],
})
export class PersonSearchFormComponent implements OnInit {

  @Output() selectedPerson: EventEmitter<any> = new EventEmitter<any>();
  personSearchResults: Person[] = [];
  personTypes: IPersonType[];
  showResults: boolean = false;
  showMore: boolean = false;

/*  data: any[] = [
    { sku: '1452-2', item: 'Pork Chops', price: 32.11 },
    { sku: '1421-0', item: 'Prime Rib', price: 41.15 },
  ];*/
  data: any[] = this.personSearchResults;

  columns: ITdDataTableColumn[] = [
    { name: 'personId', label: 'Person ID', tooltip: 'Unique Mine No' },
    { name: 'name', label: 'Person Name' },
    { name: 'firstname', label: 'Firstname'},
    { name: 'occupation', label: 'Occupation'},
    { name: 'shaft', label: 'Shaft'},
    { name: 'shift', label: 'Shift'},
  ];

  filteredData: any[] = this.data;
  filteredTotal: number = this.data.length;

  selectable: boolean = false;
  clickable: boolean = true;
  searchTerm: string = '';
  fromRow: number = 1;
  currentPage: number = 1;
  pageSize: number = 5;
  sortBy: string = 'personId';
  selectedRows: any[] = [];
  sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;

  constructor(private personService: PersonService,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef,
              private _dataTableService: TdDataTableService) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.loadPersonTypes();
    this.filter();
  }

  searchPerson(formInput: any): void {
    this.personService.searchPerson(formInput)
      .subscribe((response: any) => this.handlePersonSearchResults(response),
        (error: any) => this.toastr.error('There was an error in the search', error));
  }

  handlePersonSearchResults(results: any[]): void {
    if (results.length === 1) {
      this.selectedPerson.emit(results[0]);
    }
    if (results.length > 1 || results.length === 0) {
      this.personSearchResults = results;
      this.data = this.personSearchResults;
      this.filter();
      this.showResults = true;
    }
  }

  selectResult(selectedPerson: any): void {
    this.selectedPerson.emit(selectedPerson.row);
    this.showResults = false;
  }

  loadPersonTypes(): void {
    this.personService.getPersonTypes()
      .subscribe((response: any) => this.personTypes = response);
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.filter();
  }

  filter(): void {
    let newData: any[] = this.data;
    let excludedColumns: string[] = this.columns
      .filter((column: ITdDataTableColumn) => {
        return ((column.filter === undefined && column.hidden === true) ||
        (column.filter !== undefined && column.filter === false));
      }).map((column: ITdDataTableColumn) => {
        return column.name;
      });
    newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
    this.filteredTotal = newData.length;
    newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData = newData;
  }

}
