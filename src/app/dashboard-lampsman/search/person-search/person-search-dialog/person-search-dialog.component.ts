import {Component, EventEmitter, OnInit, Output, ViewContainerRef} from '@angular/core';
import {PersonService} from '../../../../../services/person.service';
import {Person} from '../../../../../models/person';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-person-search-dialog',
  templateUrl: './person-search-dialog.component.html',
  styleUrls: ['./person-search-dialog.component.scss'],
})
export class PersonSearchDialogComponent implements OnInit {
  @Output() selectedPerson: EventEmitter<any> = new EventEmitter<any>();
  personSearchResults: Person[];
  showResults: boolean = false;
  showMore: boolean = false;

  constructor(private personService: PersonService,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

  searchPerson(formInput: any): void {
    this.personService.searchPerson(formInput)
      .subscribe((response: any) => this.handlePersonSearchResults(response),
        (error: any) => this.toastr.error('There was an error in the search', error));
  }

  handlePersonSearchResults(results: any[]): void {
    if (results.length === 1) {
      this.selectedPerson.emit(results[0]);
    }
    if (results.length > 1) {
      this.personSearchResults = results;
      this.showResults = true;
    }
  }

  selectResult(selectedPerson: Person): void {
    this.selectedPerson.emit(selectedPerson);
    this.showResults = false;
  }

}
