import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../../../services/person.service';
import { SearchPerson } from '../../../../models/search-person';
import { Person } from '../../../../models/person';
import { Assignment } from '../../../../models/assignment';
import { ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';
import { AssetService } from '../../../../services/asset.service';
import { MdDialog } from '@angular/material';
import { AssetSearchDialogComponent } from '../asset-search/asset-search-dialog/asset-search-dialog.component';

@Component({
  selector: 'qs-person-search',
  templateUrl: './person-search.component.html',
  styleUrls: ['./person-search.component.scss'],
})
export class PersonSearchComponent implements OnInit {
  search: SearchPerson;
  selectedPerson: Person;
  personAssignments: Assignment[];
  showPersonInfo: boolean = false;
  subscription: any;

  constructor(private personService: PersonService,
              private assetService: AssetService,
              public toastr: ToastsManager,
              private route: ActivatedRoute,
              public dialog: MdDialog) {
  }

  ngOnInit(): void {
    this.search = new SearchPerson();

    let key: string = 'person';
    this.route.queryParams
      .subscribe((params: any) => {
        if (params[key]) {
          this.fetchPerson(params[key]);
        }
      });

    this.subscription = this.personService.personAssignments.subscribe(
      (selection: any) => {
        this.personAssignments = selection;
      },
    );

    this.personService.selectedPerson.subscribe((selection: any) => {
      this.selectedPerson = selection;
    });
  }

  clearSearch(): void {
    this.showPersonInfo = false;
    this.selectedPerson = undefined;
    this.personAssignments = [];
    this.personService.updateSelectedPerson(this.selectedPerson);
    this.personService.updatePersonAssignments(this.personAssignments);
  }

  handleSelectedPerson(result: any): void {
    if (result !== undefined) {
      this.selectedPerson = result;
      this.showPersonInfo = true;
      this.personService.updateSelectedPerson(this.selectedPerson);
      this.personService.getAssignmentsForPerson(this.selectedPerson.personId)
        .subscribe((assignments: Assignment[]) => {
            this.personAssignments = assignments;
            this.personService.updatePersonAssignments(assignments);
          },
          (error: any) => this.toastr.error(error));
    }
  }

  fetchPerson(personId: string): void {
    this.personService.getPerson(personId)
      .subscribe((response: Person) => this.handleSelectedPerson(response));
  }

  deallocate(assignment: Assignment): void {
    this.assetService.deassignEquipment(assignment)
      .subscribe((assignments: Assignment[]) => {
          this.personAssignments = assignments;
          this.personService.updatePersonAssignments(assignments);
        },
        (error: any) => this.toastr.error(error),
        () => this.toastr.success('Asset was successfully deallocated', 'Success!'));
  }

  openSearchAssetDialog(): any {
    let dialogRef: any = this.dialog.open(AssetSearchDialogComponent);
  }

}
