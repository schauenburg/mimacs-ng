import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSearchFormComponent } from './asset-search-form.component';

describe('AssetSearchFormComponent', () => {
  let component: AssetSearchFormComponent;
  let fixture: ComponentFixture<AssetSearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetSearchFormComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
