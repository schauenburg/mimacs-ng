import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Asset} from '../../../../models/asset';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-asset-search',
  templateUrl: './asset-search.component.html',
  styleUrls: ['./asset-search.component.scss'],
})
export class AssetSearchComponent implements OnInit {
  selectedAsset: Asset;
  showAssetInfo: boolean = false;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

  handleSelectedAsset(asset: any): void {
    if (asset !== undefined) {
      this.selectedAsset = asset;
      this.showAssetInfo = true;
    }
  }

}
