import {Component, EventEmitter, OnInit, Output, ViewContainerRef} from '@angular/core';
import {Asset} from '../../../../../models/asset';
import {AssetService} from '../../../../../services/asset.service';
import {IAssetType} from '../../../../../models/asset-type';
import {LookupService} from '../../../../../services/lookup.service';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-asset-search-dialog',
  templateUrl: './asset-search-dialog.component.html',
  styleUrls: ['./asset-search-dialog.component.scss'],
})
export class AssetSearchDialogComponent implements OnInit {
  @Output() selectedAsset: EventEmitter<any> = new EventEmitter<any>();
  assetSearchResults: Asset[];
  showResults: boolean = false;
  assetTypes: IAssetType[];
  assetStatuses: any[];

  constructor(private assetService: AssetService,
              private lookupService: LookupService,
              public toastr: ToastsManager,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    this.loadAssetTypes();
    this.loadAssetStatuses();
  }

  searchAsset(formInput: any): void {
    this.assetService.searchEquipment(formInput)
      .subscribe((response: any) => this.handleAssetSearchResults(response),
        (error: any) => this.toastr.error('There was an error in the search', 'Error!'));
  }

  handleAssetSearchResults(results: any[]): void {
    if (results.length === 1) {
      this.selectedAsset.emit(results[0]);
    }
    if (results.length > 1) {
      this.assetSearchResults = results;
      this.showResults = true;
    }
  }

  selectResult(selectedAsset: Asset): void {
    this.selectedAsset.emit(selectedAsset);
    this.showResults = false;
  }

  loadAssetTypes(): void {
    this.assetService.getEquipmentTypes()
      .subscribe((data: any[]) =>
          this.assetTypes = data,
        (error: any) => this.toastr.error(error));
  }

  loadAssetStatuses(): void {
    this.lookupService.getLookupList('assetStatus')
      .subscribe((response: any[]) => this.assetStatuses = response,
        (error: any) => this.toastr.error(error));
  }

}
