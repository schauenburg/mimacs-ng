import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpiryblockComponent } from './expiryblock.component';

describe('ExpiryblockComponent', () => {
  let component: ExpiryblockComponent;
  let fixture: ComponentFixture<ExpiryblockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpiryblockComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpiryblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
