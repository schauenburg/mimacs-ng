import {Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {Person} from '../../../models/person';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-expiryblock',
  templateUrl: './expiryblock.component.html',
  styleUrls: ['./expiryblock.component.scss'],
})
export class ExpiryblockComponent implements OnInit {
  @Input() person: Person;

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

  addInductionExpiry(): void {
    // todo
  }

  addMedicalExpiry(): void {
    // todo
  }

  addTrainingExpiry(): void {
    // todo
  }
}
