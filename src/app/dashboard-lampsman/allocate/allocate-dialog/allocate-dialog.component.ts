import { Component, Inject, OnInit } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { AssetService } from '../../../../services/asset.service';
import { Asset } from '../../../../models/asset';
import {
  IPageChangeEvent, ITdDataTableColumn, ITdDataTableSortChangeEvent, TdDataTableService,
  TdDataTableSortingOrder,
} from '@covalent/core';

@Component({
  selector: 'qs-allocate-dialog',
  templateUrl: './allocate-dialog.component.html',
  styleUrls: ['./allocate-dialog.component.scss'],
})
export class AllocateDialogComponent implements OnInit {
  availablePermanents: Asset[] = [];

  columns: ITdDataTableColumn[] = [
    {name: 'assetId', label: 'Asset ID', tooltip: 'Unique Asset No'},
    {name: 'assetType.description', label: 'Asset Type'},
    {name: 'assignmentType', label: 'Assignment Type'},
    {name: 'status', label: 'Status'},
    {name: 'allocStatus', label: 'Assigned'},
  ];

  filteredData: any[] = this.availablePermanents;
  filteredTotal: number = this.availablePermanents.length;

  selectable: boolean = false;
  clickable: boolean = true;
  multiple: boolean = false;
  searchTerm: string = '';
  fromRow: number = 1;
  currentPage: number = 1;
  pageSize: number = 5;
  sortBy: string = 'assetId';
  selectedRows: any[] = [];
  sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;

  constructor(@Inject(MD_DIALOG_DATA) public data: any,
              @Inject(MD_DIALOG_DATA) public height: any,
              @Inject(MD_DIALOG_DATA) public width: any,
              private assetService: AssetService,
              private _dataTableService: TdDataTableService,
              private dialogRef: MdDialogRef<AllocateDialogComponent>) {
  }

  /*
  * data is a string array which holds 3 key inputs
  * data[0] is a Permanent/Spare
  * data[1] is isSpare with values true or false as a string
  * data[2] assetType
  * */
  ngOnInit(): void {
    this.assetService.getEquipmentByTypeAndAllocationStatus(this.data[2], this.data[1], 'false')
      .subscribe((response: Asset[]) => {
        this.availablePermanents = response;
        this.filter();
      });
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.filter();
  }

  filter(): void {
    let newData: any[] = this.availablePermanents;
    let excludedColumns: string[] = this.columns
      .filter((column: ITdDataTableColumn) => {
        return ((column.filter === undefined && column.hidden === true) ||
        (column.filter !== undefined && column.filter === false));
      }).map((column: ITdDataTableColumn) => {
        return column.name;
      });
    newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
    this.filteredTotal = newData.length;
    newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData = newData;
  }

  selectResult(selectedAsset: any): void {
    this.dialogRef.close({asset: selectedAsset.row});
  }

}
