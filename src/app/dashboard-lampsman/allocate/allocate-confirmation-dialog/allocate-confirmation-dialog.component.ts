import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';

@Component({
  selector: 'qs-allocate-confirmation-dialog',
  templateUrl: './allocate-confirmation-dialog.component.html',
  styleUrls: ['./allocate-confirmation-dialog.component.scss'],
})
export class AllocateConfirmationDialogComponent {

  constructor(@Inject(MD_DIALOG_DATA) public data: any,
              @Inject(MD_DIALOG_DATA) public height: any,
              @Inject(MD_DIALOG_DATA) public width: any,
              public dialogRef: MdDialogRef<AllocateConfirmationDialogComponent>) { }

}
