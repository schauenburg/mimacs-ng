import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocateConfirmationDialogComponent } from './allocate-confirmation-dialog.component';

describe('AllocateConfirmationDialogComponent', () => {
  let component: AllocateConfirmationDialogComponent;
  let fixture: ComponentFixture<AllocateConfirmationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocateConfirmationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocateConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
