import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-recently-deallocated',
  templateUrl: './recently-deallocated.component.html',
  styleUrls: ['./recently-deallocated.component.scss'],
})
export class RecentlyDeallocatedComponent implements OnInit {

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
