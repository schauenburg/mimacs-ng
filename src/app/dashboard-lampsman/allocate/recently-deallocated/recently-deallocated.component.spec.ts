import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentlyDeallocatedComponent } from './recently-deallocated.component';

describe('RecentlyDeallocatedComponent', () => {
  let component: RecentlyDeallocatedComponent;
  let fixture: ComponentFixture<RecentlyDeallocatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentlyDeallocatedComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentlyDeallocatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
