import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {IStepChangeEvent, StepState} from '@covalent/core';
import {Person} from '../../../models/person';
import {Asset} from '../../../models/asset';
import {Assignment} from '../../../models/assignment';
import {AssetService} from '../../../services/asset.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PersonService} from '../../../services/person.service';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-allocate',
  templateUrl: './allocate.component.html',
  styleUrls: ['./allocate.component.scss'],
})
export class AllocateComponent implements OnInit {
  activeDeactiveStep1Msg: string = 'No select/deselect detected yet';
  stateStep1: StepState = StepState.None;
  stateStep2: StepState = StepState.Required;
  stateStep3: StepState = StepState.None;

  disabled: boolean = false;

  activeStep1: boolean = true;
  activeStep2: boolean = false;
  activeStep3: boolean = false;

  showPersonInfo: boolean = false;
  showAssetInfo: boolean = false;

  selectedPerson: Person = undefined;
  selectedAsset: Asset = undefined;
  recentAssignment: Assignment;

  constructor(private assetService: AssetService,
              private personService: PersonService,
              public toastr: ToastsManager,
              private  route: ActivatedRoute,
              private router: Router,
              private vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    let key: string = 'person';
    this.route.queryParams
      .subscribe((params: any) => {
        if (params[key]) {
          this.fetchPerson(params[key]);
        }
        key = 'asset';
        if (params[key]) {
          this.fetchAsset(params[key]);
        }
      });
  }

  toggleRequiredStep2(): void {
    this.stateStep2 = (this.stateStep2 === StepState.Required ? StepState.None : StepState.Required);
  }

  toggleCompleteStep3(): void {
    this.allocate();
    this.stateStep3 = (this.stateStep3 === StepState.Complete ? StepState.None : StepState.Complete);
  }

  activeStep1Event(): void {
    this.activeDeactiveStep1Msg = 'Active event emitted.';
  }

  deactiveStep1Event(): void {
    this.activeDeactiveStep1Msg = 'Deactive event emitted.';
    if (this.selectedPerson === undefined) {
      this.stateStep1 = StepState.Required;
    }
  }

  change(event: IStepChangeEvent): void {
    // todo see if there are things to do here
  }

  clearSearch(): void {
    this.selectedPerson = undefined;
  }

  handleSelectedPerson(person: Person): void {
    if (person !== undefined) {
      this.selectedPerson = person;
      this.stateStep1 = StepState.Complete;
      this.activeStep1 = false;
      this.activeStep2 = true;
      this.showPersonInfo = true;
    }
  }

  handleSelectedAsset(asset: Asset): void {
    if (asset !== undefined) {
      this.selectedAsset = asset;
      this.showAssetInfo = true;
      this.stateStep2 = StepState.Complete;
      this.activeStep2 = false;
      if (this.selectedPerson !== undefined) {
        this.activeStep3 = true;
      } else {
        this.activeStep1 = true;
      }

    }
  }

  allocate(): void {
    if (this.selectedPerson !== undefined && this.selectedAsset !== undefined) {
      let assignment: Assignment = new Assignment(this.selectedAsset, this.selectedPerson);
      // let data = JSON.stringify(assignment);
      this.assetService.assignEquipment(assignment)
        .subscribe((response: Assignment[]) => this.personService.updatePersonAssignments(response),
          (error2: any) => this.toastr.error(error2),
          () => {
            this.toastr.success('Allocation was successful', 'Success!');
            this.router.navigate(['/lampsman/search'], {queryParams: {person: this.selectedPerson.personId}});
          });
    }
  }

  fetchPerson(personId: string): void {
    this.personService.getPerson(personId)
      .subscribe((response: Person) => this.handleSelectedPerson(response));
  }

  fetchAsset(assetId: string): void {
    this.assetService.getEquipment(assetId)
      .subscribe((response: Asset) => this.handleSelectedAsset(response));
  }
}
