import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentlyAllocatedComponent } from './recently-allocated.component';

describe('RecentlyAllocatedComponent', () => {
  let component: RecentlyAllocatedComponent;
  let fixture: ComponentFixture<RecentlyAllocatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentlyAllocatedComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentlyAllocatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
