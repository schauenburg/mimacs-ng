import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'qs-recently-allocated',
  templateUrl: './recently-allocated.component.html',
  styleUrls: ['./recently-allocated.component.scss'],
})
export class RecentlyAllocatedComponent implements OnInit {

  constructor(private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
  }

}
