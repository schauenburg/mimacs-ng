import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {AssetService} from '../../../services/asset.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {Asset} from '../../../models/asset';
import {IAttribute} from '../../../models/attribute';

@Component({
  selector: 'qs-edit-asset',
  templateUrl: './edit-asset.component.html',
  styleUrls: ['./edit-asset.component.scss'],
})
export class EditAssetComponent implements OnInit {
  asset: Asset;
  editAssetForm: FormGroup;
  assetTypeAttributes: IAttribute[];

  constructor(private assetService: AssetService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit(): void {
    let key: string = 'id';
    this.route.params.subscribe((params: any) => {
      if (params[key]) {
        this.fetchAsset(params[key]);
      }
    });
  }

  fetchAsset(assetId: string): void {
    this.assetService.getEquipment(assetId)
      .subscribe((response: any) => {
        this.asset = response;
        this.getAssetTypeAttributes(this.asset.assetId);
      });
  }

  getAssetTypeAttributes(name: string): void {
    this.assetService.getEquipmentTypeAttributes(name)
      .subscribe((attributes: any[]) => {
          this.assetTypeAttributes = attributes;
        },
        (error: any) => this.toastr.error(error),
        () => {
          this.initFormModel();
        });
  }

  attributeMapFormGroup(): FormGroup {
    let attribFormGroup: FormGroup = new FormGroup({});
    for (let attrib of this.asset.attr) {
      alert(attrib.name + ': ' + attrib.value);
      attribFormGroup.addControl(attrib.name, new FormControl(attrib.value));
    }
    return attribFormGroup;
  }

  initFormModel(): void {
    alert(this.asset.type.description);
    this.editAssetForm = this.formBuilder.group({
      assetId: new FormControl({value: this.asset.assetId, disabled: true}),
      type: new FormControl({value: this.asset.type, disabled: true}),
      attributeMap: this.attributeMapFormGroup(),
    });
  }

}
