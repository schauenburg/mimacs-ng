import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qs-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  currentDate: any;
  icon: string = 'assets:teradata-ux';

  ngOnInit(): void {
    this.currentDate = Date.now();
  }
}
