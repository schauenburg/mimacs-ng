import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SystemFeaturesComponent } from './dashboard-admin/features/features.component';
import { FeaturesFormComponent } from './dashboard-lampsman/features/form/form.component';
import { LogsComponent } from './logs/logs.component';
import { DetailComponent } from './detail/detail.component';
import { LoginComponent } from './login/login.component';
import { FormComponent } from './form/form.component';
import { AdminOverviewComponent } from './dashboard-admin/overview/overview.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: MainComponent,
    children: [
      {
        component: DashboardComponent,
        path: '',
      },
      {
        path: 'item/:id',
        component: DetailComponent,
      },
      {
        path: 'logs',
        component: LogsComponent,
      },
      {
        path: 'form',
        component: FormComponent,
      },
      {path: '', loadChildren: './users/users.module#UsersModule'},
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: true}),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}
export const routedComponents: any[] = [
  MainComponent, LoginComponent,
  DashboardComponent,
  FormComponent, LogsComponent, DetailComponent,
  FeaturesFormComponent, SystemFeaturesComponent, AdminOverviewComponent,
];
