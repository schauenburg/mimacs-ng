import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { RepairsDashboardComponent } from './repairs-dashboard/repairs-dashboard.component';
import { RepairsRoutingModule } from './repairs.routing.module';
import { SharedModule } from '../shared/shared.module';
import { InspectionComponent } from './inspection/inspection.component';
import { BookedInComponent } from './booked-in/booked-in.component';
import { InRepairComponent } from './in-repair/in-repair.component';
import { CompletedRepairsComponent } from './completed-repairs/completed-repairs.component';
import { BerComponent } from './ber/ber.component';
import { QuarantineComponent } from './quarantine/quarantine.component';
import { RepairComponent } from './repair/repair.component';

@NgModule({
  declarations: [
    RepairsDashboardComponent,
    InspectionComponent,
    BookedInComponent,
    InRepairComponent,
    CompletedRepairsComponent,
    BerComponent,
    QuarantineComponent,
    RepairComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    JsonpModule,
    SharedModule,
    RepairsRoutingModule],
  providers: [],
})
export class RepairsModule {}
