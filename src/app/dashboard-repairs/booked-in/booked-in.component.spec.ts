import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookedInComponent } from './booked-in.component';

describe('BookedInComponent', () => {
  let component: BookedInComponent;
  let fixture: ComponentFixture<BookedInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookedInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookedInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
