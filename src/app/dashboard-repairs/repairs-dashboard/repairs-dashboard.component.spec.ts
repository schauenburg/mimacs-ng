import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairsDashboardComponent } from './repairs-dashboard.component';

describe('RepairsDashboardComponent', () => {
  let component: RepairsDashboardComponent;
  let fixture: ComponentFixture<RepairsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
