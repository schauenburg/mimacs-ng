import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InRepairComponent } from './in-repair.component';

describe('InRepairComponent', () => {
  let component: InRepairComponent;
  let fixture: ComponentFixture<InRepairComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InRepairComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InRepairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
