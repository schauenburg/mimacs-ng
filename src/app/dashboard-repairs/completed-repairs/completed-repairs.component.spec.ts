import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedRepairsComponent } from './completed-repairs.component';

describe('CompletedRepairsComponent', () => {
  let component: CompletedRepairsComponent;
  let fixture: ComponentFixture<CompletedRepairsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedRepairsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedRepairsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
