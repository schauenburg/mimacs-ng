import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepairsDashboardComponent } from './repairs-dashboard/repairs-dashboard.component';
import { MainComponent } from '../main/main.component';
import { InspectionComponent } from './inspection/inspection.component';
import { BookedInComponent } from './booked-in/booked-in.component';
import { InRepairComponent } from './in-repair/in-repair.component';
import { CompletedRepairsComponent } from './completed-repairs/completed-repairs.component';
import { BerComponent } from './ber/ber.component';
import { QuarantineComponent } from './quarantine/quarantine.component';
import { RepairComponent } from './repair/repair.component';

const repairRoutes: Routes = [
  {
    path: 'repairs',
    component: MainComponent,
    canActivate: [],
    children: [
      {
        component: RepairsDashboardComponent,
        path: '',
        children: [
          {
            path: '',
            component: RepairComponent,
            children: [
              {
                path: '',
                component: InspectionComponent,
              },
              {
                path: 'booked',
                component: BookedInComponent,
              },
              {
                path: 'inrepair',
                component: InRepairComponent,
              },
              {
                path: 'complete',
                component: CompletedRepairsComponent,
              },
              {
                path: 'ber',
                component: BerComponent,
              },
              {
                path: 'quarantine',
                component: QuarantineComponent,
              },
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(repairRoutes)],
  exports: [RouterModule],
})
export class RepairsRoutingModule {}
