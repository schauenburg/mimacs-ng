import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BerComponent } from './ber.component';

describe('BerComponent', () => {
  let component: BerComponent;
  let fixture: ComponentFixture<BerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
