import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SettingsComponent } from './settings/settings.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { UsersComponent } from './user-admin/users/users.component';
import { RolesComponent } from './user-admin/roles/roles.component';
import { PermissionsComponent } from './user-admin/permissions/permissions.component';
import { CreateUserComponent } from './user-admin/create-user/create-user.component';
import { EquipmentAdminComponent } from './equipment/equipment.component';
import { LampsmanAdminComponent } from './lampsman/lampsman.component';
import { LocationsAdminComponent } from './locations/locations.component';
import { EventTypesAdminComponent } from './event-types/event-types.component';
import { PersonelAdminComponent } from './personel/personel.component';
import { RepairAdminComponent } from './repair/repair.component';
import { LifeCyclesAdminComponent } from './life-cycles/life-cycles.component';
import { DashboardAdminComponent } from './dashboard-admin.component';
import { MainComponent } from '../main/main.component';
import { AdminOverviewComponent } from './overview/overview.component';
import {MonitoringComponent} from "./monitoring/monitoring.component";

const adminRoutes: Routes = [
  { path: 'admin',
    component: MainComponent,
    canActivate: [],
    children: [
      {
        component: DashboardAdminComponent,
        path: '',
        children: [
          {
            path: '',
            component: AdminOverviewComponent,
            children: [
              {
                path: '',
                component: UserAdminComponent,
                children: [
                  {
                    path: '',
                    component: UsersComponent,
                    children: [
                      {
                        path: 'user/create',
                        component: CreateUserComponent,
                      },
                    ],
                  },
                  {
                    path: 'user/roles',
                    component: RolesComponent,
                  },
                  {
                    path: 'user/permissions',
                    component: PermissionsComponent,
                  },
                ],
              },
              {
                path: 'settings',
                component: SettingsComponent,
              },
              {
                path: 'equipment',
                component: EquipmentAdminComponent,
              },
              {
                path: 'lampsman',
                component: LampsmanAdminComponent,
              },
              {
                path: 'locations',
                component: LocationsAdminComponent,
              },
              {
                path: 'event-types',
                component: EventTypesAdminComponent,
              },
              {
                path: 'personel',
                component: PersonelAdminComponent,
              },
              {
                path: 'repair',
                component: RepairAdminComponent,
              },
              {
                path: 'life-cycles',
                component: LifeCyclesAdminComponent,
              },
              {
                path: 'monitoring',
                component: MonitoringComponent,
              },
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
