import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelAdminComponent } from './personel.component';

describe('PersonelAdminComponent', () => {
  let component: PersonelAdminComponent;
  let fixture: ComponentFixture<PersonelAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonelAdminComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonelAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
