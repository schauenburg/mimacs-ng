import {Component, OnInit} from '@angular/core';
import {IService} from '../../../models/service';
import {Title} from '@angular/platform-browser';
import {TdDialogService, TdLoadingService} from '@covalent/core';
import {ServicesService} from '../../../services/services.service';

@Component({
  selector: 'qs-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss'],
})
export class ServicesComponent implements OnInit {

  services: IService[];
  filteredServices: IService[];

  constructor(private _titleService: Title,
              private _dialogService: TdDialogService,
              private servicesService: ServicesService,
              private _loadingService: TdLoadingService) {

  }

  openConfirm(id: string): void {
    this._dialogService.openConfirm({
      message: 'Are you sure you want to delete this service? It\'s being used!',
      title: 'Confirm',
      cancelButton: 'No, Cancel',
      acceptButton: 'Yes, Delete',
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.deleteFeature(id);
      } else {
        // DO SOMETHING ELSE
      }
    });
  }

  ngOnInit(): void {
    this._titleService.setTitle('Product Features');
    this.loadFeatures();
  }

  filterFeatures(filterTitle: string = ''): void {
    this.filteredServices = this.services.filter((service: IService) => {
      return service.name.toLowerCase().indexOf(filterTitle.toLowerCase()) > -1;
    });
  }

  loadFeatures(): void {
    this._loadingService.register('services.list');
    this.servicesService.query().subscribe((services: IService[]) => {
      this.services = services;
      this.filteredServices = services;
      this._loadingService.resolve('services.list');
    }, (error: Error) => {
      this.servicesService.staticQuery().subscribe((services: IService[]) => {
        this.services = services;
        this.filteredServices = services;
        this._loadingService.resolve('services.list');
      });
    });
  }

  deleteFeature(id: any): void {
    this._loadingService.register('services.list');
    this.servicesService.delete(id).subscribe(() => {
      this.services = this.services.filter((service: IService) => {
        return service.id !== id;
      });
      this.filteredServices = this.filteredServices.filter((service: IService) => {
        return service.id !== id;
      });
      this._loadingService.resolve('services.list');
    }, (error: Error) => {
      this._loadingService.resolve('services.list');
    });
  }

}
