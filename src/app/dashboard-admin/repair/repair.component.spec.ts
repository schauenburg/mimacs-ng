import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairAdminComponent } from './repair.component';

describe('RepairAdminComponent', () => {
  let component: RepairAdminComponent;
  let fixture: ComponentFixture<RepairAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepairAdminComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
