import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin.routing.module';
import { DashboardAdminComponent } from './dashboard-admin.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { SettingsComponent } from './settings/settings.component';
import { EquipmentAdminComponent } from './equipment/equipment.component';
import { LampsmanAdminComponent } from './lampsman/lampsman.component';
import { LocationsAdminComponent } from './locations/locations.component';
import { EventTypesAdminComponent } from './event-types/event-types.component';
import { PersonelAdminComponent } from './personel/personel.component';
import { RepairAdminComponent } from './repair/repair.component';
import { LifeCyclesAdminComponent } from './life-cycles/life-cycles.component';
import { UsersComponent } from './user-admin/users/users.component';
import { RolesComponent } from './user-admin/roles/roles.component';
import { PermissionsComponent } from './user-admin/permissions/permissions.component';
import { CreateUserComponent } from './user-admin/create-user/create-user.component';
import { SharedModule } from '../shared/shared.module';
import { FooterComponent } from '../layout/footer/footer.component';
@NgModule({
  declarations: [
    DashboardAdminComponent,
    UserAdminComponent,
    SettingsComponent,
    EquipmentAdminComponent,
    LampsmanAdminComponent,
    LocationsAdminComponent,
    EventTypesAdminComponent,
    PersonelAdminComponent,
    RepairAdminComponent,
    LifeCyclesAdminComponent,
    UsersComponent,
    RolesComponent,
    PermissionsComponent,
    CreateUserComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AdminRoutingModule,
  ],
})
export class AdminModule {}
