import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeCyclesAdminComponent } from './life-cycles.component';

describe('LifeCyclesAdminComponent', () => {
  let component: LifeCyclesAdminComponent;
  let fixture: ComponentFixture<LifeCyclesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeCyclesAdminComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeCyclesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
