import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventTypesAdminComponent } from './event-types.component';

describe('EventTypesAdminComponent', () => {
  let component: EventTypesAdminComponent;
  let fixture: ComponentFixture<EventTypesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventTypesAdminComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventTypesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
