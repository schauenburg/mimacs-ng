import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LampsmanAdminComponent } from './lampsman.component';

describe('LampsmanAdminComponent', () => {
  let component: LampsmanAdminComponent;
  let fixture: ComponentFixture<LampsmanAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LampsmanAdminComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LampsmanAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
