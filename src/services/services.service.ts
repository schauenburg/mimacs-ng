import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpInterceptorService, RESTService } from '@covalent/http';
import { MOCK_API } from '../config/api.config';
import {IService} from '../models/service';

export interface IFeature {
  title: string;
  id: string;
  user: string;
  modified: Date;
  created: Date;
  icon: string;
  enabled: number;
}

@Injectable()
export class ServicesService extends RESTService<IService> {

  constructor(private _http: HttpInterceptorService) {
    super(_http, {
      baseUrl: MOCK_API,
      path: '/services',
    });
  }

  staticQuery(): Observable<IService[]> {
    return this._http.get('data/services.json')
    .map((res: Response) => {
      return res.json();
    });
  }
}
