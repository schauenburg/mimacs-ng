import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Person } from '../models/person';
import { IPersonType } from '../models/person-type';
import { Assignment } from '../models/assignment';
import { Observer } from 'rxjs/Observer';

@Injectable()
export class PersonService {
  private personBaseUrl: string = 'http://localhost:8080/rest/person/';
  private selectedPersonObserver: Observer<Person>;
  private assignmentsObserver: Observer<Assignment[]>;
  public selectedPerson: Observable<Person>;
  public personAssignments: Observable<Assignment[]>;

  constructor(private http: Http) {
    this.selectedPerson = new Observable<Person>((observer: any) =>
      this.selectedPersonObserver = observer,
    ).share();

    this.personAssignments = new Observable<Assignment[]>((observer: any) =>
      this.assignmentsObserver = observer,
    ).share();
  }

  getPerson(personId: string): Observable<any> {
    return this.http.get(this.personBaseUrl + personId, this.getHeaders())
      .map((response: any) => <any> response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  createPerson(person: any): Observable<Person> {
    return this.http.post(this.personBaseUrl + 'create', person, this.getHeaders())
      .map((response: any) => <Person> response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  editPerson(person: any): Observable<Person> {
    return this.http.put(this.personBaseUrl + 'edit', person, this.getHeaders())
      .map((response: any) => <Person> response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getPersonTypes(): Observable<IPersonType[]> {
    return this.http.get(this.personBaseUrl + 'types')
      .map((response: any) => <IPersonType[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  searchPerson(data: any): Observable<Person[]> {
    return this.http.post(this.personBaseUrl + 'search', data, this.getHeaders())
      .map((response: any) => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getAssignmentsForPerson(personId: string): Observable<Assignment[]> {
    return this.http.get(this.personBaseUrl + personId + '/assignments')
      .map((response: any) => <Assignment[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getPersonTypeAttributes(name: string): Observable<any[]> {
    return this.http.get(this.personBaseUrl + 'types/' + name + '/attributes')
      .map((response: any) => <any[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  updateSelectedPerson(selected: Person): void {
    if (this.selectedPersonObserver !== undefined) {
      this.selectedPersonObserver.next(selected);
    }
  }

  updatePersonAssignments(assignments: Assignment[]): void {
    if (this.assignmentsObserver !== undefined) {
      this.assignmentsObserver.next(assignments);
    }
  }

  private getHeaders(): any {
    const headerDict: any = {
      'content-type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
    };

    const headers: any = new Headers(headerDict);
    const options: any = new RequestOptions({headers: headers});
    return options;
  }
}
