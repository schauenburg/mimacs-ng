import {Injectable} from '@angular/core';
import {Http, RequestOptions} from '@angular/http';
import {ISelectItem} from '../models/select-item';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LookupService {
  private lookupBaseUrl: string = 'http://localhost:8080/rest/lookup/';

  constructor(private http: Http) {
  }

  getLookupList(name: string): Observable<ISelectItem[]> {
    return this.http.get(this.lookupBaseUrl + name)
      .map((response: any) => <any[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  private getHeaders(): RequestOptions {
    let headers: any = new Headers({'Content-Type': 'application/json;charset=utf-8'});
    headers.append('Accept', 'application/json');
    let options: any = new RequestOptions({headers: headers});
    return options;
  }
}
