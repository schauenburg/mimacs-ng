import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Asset } from '../models/asset';
import { Person } from '../models/person';
import { Assignment } from '../models/assignment';
import { IAssetType } from '../models/asset-type';
@Injectable()
export class AssetService {
  private equipmentBaseUrl: string = 'http://localhost:8080/rest/equipment/';

  constructor(private http: Http) {
  }

  getEquipment(assetId: string): Observable<Asset> {
    return this.http.get(this.equipmentBaseUrl + assetId, this.getHeaders())
      .map((response: any) => <Asset> response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getEquipmentByTypeAndAllocationStatus(assetType: string, isSpare: string, isAssigned: string): Observable<Asset[]> {
    return this.http.get(this.equipmentBaseUrl + assetType + '/' + isSpare + '/' + isAssigned, this.getHeaders())
      .map((response: any) => <Asset[]> response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  assignEquipment(assignment: any): Observable<Assignment[]> {
    return this.http.post(this.equipmentBaseUrl + 'assign', assignment, this.getHeaders())
      .map((response: any) => <Assignment[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  deassignEquipment(assignment: any): Observable<Assignment[]> {
    return this.http.put(this.equipmentBaseUrl + 'deassign', assignment, this.getHeaders())
      .map((response: any) => <Assignment[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  createEquipment(equipment: any): Observable<Asset> {
    return this.http.post(this.equipmentBaseUrl + 'create', equipment, this.getHeaders())
      .map((response: any) => <Asset> response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getEquipmentTypeByName(name: string): Observable<IAssetType> {
    return this.http.get(this.equipmentBaseUrl + 'types/' + name)
      .map((response: any) => <IAssetType>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getEquipmentTypes(): Observable<IAssetType[]> {
    return this.http.get(this.equipmentBaseUrl + 'types')
      .map((response: any) => <IAssetType[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getEquipmentTypeAttributes(name: string): Observable<any[]> {
    return this.http.get(this.equipmentBaseUrl + 'types/' + name + '/attributes')
      .map((response: any) => <any[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getEquipmentTypeCount(): Observable<any[]> {
    return this.http.get(this.equipmentBaseUrl + 'count')
      .map((response: any) => <any[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  searchEquipment(data: any): Observable<Asset[]> {
    return this.http.post(this.equipmentBaseUrl + 'search', data, this.getHeaders())
      .map((response: any) => <Asset[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getPersonForEquipment(assetId: string): Observable<Person> {
    return this.http.get(this.equipmentBaseUrl + assetId + '/person')
      .map((response: any) => <Person>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  getAssignmentsForEquipment(assetId: string): Observable<Assignment[]> {
    return this.http.get(this.equipmentBaseUrl + assetId + '/assignments')
      .map((response: any) => <Assignment[]>response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'))
      .share();
  }

  private getHeaders(): RequestOptions {
    let headers: any = new Headers({'Content-Type': 'application/json;charset=utf-8'});
    headers.append('Accept', 'application/json');
    let options: any = new RequestOptions({headers: headers});
    return options;
  }
}
