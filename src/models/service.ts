/**
 * Created by bright on 2017/06/30.
 */
export interface IService {
  name: string;
  id: string;
  user: string;
  modified: Date;
  created: Date;
  icon: string;
  running: boolean;
}
