import {Asset} from './asset';
import {Person} from './person';
export class Assignment {
  id: number;
  asset: Asset;
  person: Person;
  status: string;
  startDate: any;
  enabled: boolean;

  constructor(asset: Asset, person: Person) {
    this.asset = asset;
    this.person = person;
    this.enabled = true;
    this.startDate = new Date();
    this.status = 'allocated';
  }
}
