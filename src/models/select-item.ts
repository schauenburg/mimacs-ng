/**
 * Created by bright on 2017/07/12.
 */
export interface ISelectItem {
  description: string;
  label: string;
  value: any;
  escape: boolean;
  noSelectionOption: boolean;
}
