import {IAttribute} from './attribute';
import {IAssetType} from './asset-type';
import {IAttributeMapEntry} from './attribute-map-entry';

export class Asset {
  assetId: string;
  allocStatus: string;
  type: any;
  assetType: IAssetType;
  assignmentType: string;
  status: string;
  attr: any[];
  attributeMap: IAttributeMapEntry[];
}
