import {IAttributeMapEntry} from './attribute-map-entry';
import {IPersonType} from './person-type';
import {IAttribute} from './attribute';
export class Person {
  personId: string;
  name: string;
  firstname: string;
  occupation: string;
  personType: IPersonType;
  shaft: string;
  shift: string;
  id: number;
  type: any;
  attr: any[];
  attributeMap: IAttributeMapEntry[];
}
