import {IPersonType} from './person-type';
export class SearchPerson {
  name: string;
  personType: IPersonType;
  companyNo: string;
  identityNo: string;
}
