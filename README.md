# Mimacs Angular Frontend

LMS App with @covalent and angular material packages

## Setup

* Ensure you have Node 4.4+ and NPM 3+ installed.
* Install Angular CLI `npm i -g @angular/cli@latest`
* Install Typescript 2.x `npm i -g typescript`
* Install TSLint `npm install -g tslint`
* Install Protractor for e2e testing `npm install -g protractor`
* Install Node packages `npm i`
* Update Webdriver `webdriver-manager update` and `./node_modules/.bin/webdriver-manager update`
* Run local build `ng serve`
